# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=baloo
pkgver=5.109.0
pkgrel=0
pkgdesc="A framework for searching and managing metadata"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends_dev="
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kfilemetadata-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kjobwidgets-dev
	kservice-dev
	lmdb-dev
	qt5-qtdeclarative-dev
	solid-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/baloo.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/baloo-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-lang"
options="!check" # Tons of broken tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/baloo.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
63e92a9b3fe9b397f8f30bdd96c7ad62f564f2e768d29ab909809ea969a11129545ec53ff49b974946a685ee4010e2c153dc10b5ac6d3a525b912a8b92aed0e4  baloo-5.109.0.tar.xz
"
