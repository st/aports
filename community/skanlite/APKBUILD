# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=skanlite
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/skanlite"
pkgdesc="Lite image scanning application"
license="LicenseRef-KDE-Accepted-GPL"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	libksane-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/graphics/skanlite.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/skanlite-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8ff4bc50efc96db2ebb72bf0b9e5be66929141dee43b6a578806130e04790e06db18b24484b2a821eabfbeb71c0301f778250c4eedf5f6865be5e48abcd1afe0  skanlite-23.08.0.tar.xz
"
