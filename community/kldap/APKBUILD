# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kldap
pkgver=23.08.0
pkgrel=0
pkgdesc="LDAP access API for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcompletion-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	openldap-dev
	qtkeychain-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	cyrus-sasl-dev
	samurai
	"
_repo_url="https://invent.kde.org/pim/kldap.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kldap-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
177a8e9a37f38d15c4e05038504d996baa24305aaeefa37197323664eb48ade2384d4850323b9c1159e0a317841b3a6ec0ff96de72933c307a1614d70c4d0994  kldap-23.08.0.tar.xz
"
