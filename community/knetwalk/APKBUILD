# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=knetwalk
pkgver=23.08.0
pkgrel=0
pkgdesc="Connect all the terminals to the server, in as few turns as possible"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/knetwalk/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/knetwalk.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/knetwalk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c2b88de46bf8517708957866a0b46d3c599f5e54b6d77476d9c507f61ea9ba4dc3fe05b98d33b18c537505e1d7bc3632a870cde71930b13f9e4c93a8d03771bc  knetwalk-23.08.0.tar.xz
"
