# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kosmindoormap
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/libraries/kosmindoormap"
pkgdesc="OSM multi-floor indoor map renderer"
license="BSD-2-Clause AND BSD-3-Clause MIT AND LGPL-2.0-or-later"
depends_dev="
	ki18n-dev
	kpublictransport-dev
	protobuf-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	zlib-dev
	"
makedepends="$depends_dev
	bison
	extra-cmake-modules
	flex
	samurai
	"
_repo_url="https://invent.kde.org/libraries/kosmindoormap.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kosmindoormap-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	LDFLAGS="$LDFLAGS -Wl,--copy-dt-needed-entries" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	local skipped_tests="("
	local tests="
		"
	if [ "$CARCH" = "x86" ]; then
		tests="$tests
			levelparser
			platformfinder
			"
	fi
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
282f7cd3c0436684c3321c2c65a3782abb9175005f726a075a6461cfdf5ee64403401d2ec10f0d7d06b3f683db408fcccfd4ea5cdab1deccd222d191a6df197e  kosmindoormap-23.08.0.tar.xz
"
