# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this apkbuild by some scripts for automation
# group=kde-frameworks
pkgname=kguiaddons
pkgver=5.109.0
pkgrel=0
pkgdesc="Addons to QtGui"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	plasma-wayland-protocols
	qt5-qttools-dev
	qt5-qtwayland-dev
	qt5-qtx11extras-dev
	wayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kguiaddons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kguiaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kguiaddons.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6387f7d4acbb897107acfe67cd4b911e6789b71cd7e8b16dc0acdb0660ef1b8a32da7a21ade9919c3bf794517e8ae93a533959e3526e0b1ad9f82166d5a31a55  kguiaddons-5.109.0.tar.xz
"
