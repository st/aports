# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kfind
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/utilities/kfind"
pkgdesc="Find Files/Folders"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kfind.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kfind-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d59710ff34dc7a817d1da1d1ada96495a519953bf659e103f360ba4095c1e49bd29ac57fc0b2c1d5a4b53afba15578f7d31d18352ccf261ef2faa1aec71b2fec  kfind-23.08.0.tar.xz
"
