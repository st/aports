# Maintainer: team/kde <bribbers@disroot.org>
# Contributor: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kactivities-stats
pkgver=5.109.0
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="A library for accessing the usage data collected by the activities system"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	boost-dev
	graphviz-dev
	kactivities-dev
	kconfig-dev
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kactivities-stats.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-stats-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kactivities-stats.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
157648cf846b06834308153b38ff14c2de651005ec747cca995368381b5aa839647075af8db5bcae38189dbc69b9f3823da558e70a49df817489d897338d362e  kactivities-stats-5.109.0.tar.xz
"
