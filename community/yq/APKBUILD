# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Tuan Hoang <tmhoang@linux.ibm.com>
# Maintainer: Tuan Hoang <tmhoang@linux.ibm.com>
pkgname=yq
pkgver=4.35.1
pkgrel=0
pkgdesc="Portable command-line YAML processor written in Go"
url="https://github.com/mikefarah/yq"
arch="all"
license="MIT"
makedepends="go"
checkdepends="bash tzdata"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikefarah/yq/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o yq

	./yq shell-completion bash > yq.bash
	./yq shell-completion zsh > yq.zsh
	./yq shell-completion fish > yq.fish
}

check() {
	go test ./...

	# Yanked from scripts/acceptance.sh
	for test in acceptance_tests/*.sh; do
		echo "--------------------------------------------------------------"
		echo "$test"
		echo "--------------------------------------------------------------"
		bash "$test"
	done
}

package() {
	install -Dm755 yq "$pkgdir"/usr/bin/yq

	install -Dm644 yq.bash \
		"$pkgdir"/usr/share/bash-completion/completions/yq
	install -Dm644 yq.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_yq
	install -Dm644 yq.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/yq.fish
}

sha512sums="
e62fbb4c9b9d6156740eec668e7f81255abfad65b486f25b97ff30bcd629a8256cdd762e462b4fd6331f93334153e43f76af967ef5c7a35fcbd7dc9eb35aba3f  yq-4.35.1.tar.gz
"
