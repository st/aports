# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kblackbox
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/org.kde.kblackbox"
pkgdesc="A game of hide and seek played on a grid of boxes"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	ktextwidgets-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kblackbox.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kblackbox-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5c8f94b4a5a7d267479b0a21e8706dbdc61b470956a49f9f0e3d3a3d05f94676ee6ec0436ae86800a4456ae6d50ee6629def55f907bc80f656457d7f7d9ef8f8  kblackbox-23.08.0.tar.xz
"
