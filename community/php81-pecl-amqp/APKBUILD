# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-amqp
_extname=amqp
pkgver=2.0.0
pkgrel=0
pkgdesc="PHP 8.1 extension to communicate with any AMQP spec 0-9-1 compatible server - PECL"
url="https://pecl.php.net/package/amqp"
arch="all"
license="PHP-3.01"
_phpv=81
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev rabbitmq-c-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

install_if="php-$_extname php$_phpv"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Tests require running AMQP server, so basic check
	$_php -d extension="$builddir"/modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/40_$_extname.ini
}

sha512sums="
0f946a6ff6005389d89a5f648353ddf6b99ca4b118efc5344bad0729cabb23299b55c74d9ef122d731fe9fb7f6265affd9a40e23a4394fbece9b3903c264f0ef  php-pecl-amqp-2.0.0.tgz
"
