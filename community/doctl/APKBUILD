# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=doctl
pkgver=1.98.1
pkgrel=0
pkgdesc="Official command line interface for the DigitalOcean API"
url="https://github.com/digitalocean/doctl"
license="Apache-2.0"
arch="all"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/digitalocean/doctl/archive/v$pkgver/doctl-$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	maj_min=${pkgver%*.*}
	major=${maj_min%.*}
	minor=${maj_min#*.}
	patch=${pkgver#*.*.*}

	go build -v \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\" \
			-X github.com/digitalocean/doctl.Major=$major \
			-X github.com/digitalocean/doctl.Minor=$minor \
			-X github.com/digitalocean/doctl.Patch=$patch \
			-X github.com/digitalocean/doctl.Label=alpine-$pkgrel" \
		./cmd/...
}

check() {
	go test -mod=readonly ./integration
}

package() {
	install -Dm755 doctl -t "$pkgdir"/usr/bin/

	# setup completions
	mkdir -p "$pkgdir"/usr/share/bash-completion/completions \
		"$pkgdir"/usr/share/zsh/site-functions \
		"$pkgdir"/usr/share/fish/vendor_completions.d

	"$pkgdir"/usr/bin/doctl completion bash > "$pkgdir"/usr/share/bash-completion/completions/doctl
	"$pkgdir"/usr/bin/doctl completion zsh > "$pkgdir"/usr/share/zsh/site-functions/_doctl
	"$pkgdir"/usr/bin/doctl completion fish > "$pkgdir"/usr/share/fish/vendor_completions.d/doctl.fish
}

sha512sums="
2858e835a50996123d9532fd05998b007db42ce1273877bfc7c17c2666d1d90887e9804cba33ede4c3466944383228709cb675592afeca4b82c1a478af05add6  doctl-1.98.1.tar.gz
"
