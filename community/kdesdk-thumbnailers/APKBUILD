# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdesdk-thumbnailers
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/development/"
pkgdesc="Plugins for the thumbnailing system"
license="GPL-2.0-only OR GPL-3.0-only"
makedepends="
	extra-cmake-modules
	gettext-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kdesdk-thumbnailers.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdesdk-thumbnailers-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
23293f7b4f0822098abb6341656d0d22dca6ab2cedd592618f1204a6d1897749bd99a7554d88f438736798f7151cf47a93de59738e4241e7b45bfd389bac0431  kdesdk-thumbnailers-23.08.0.tar.xz
"
