# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=marble
pkgver=23.08.0
pkgrel=0
pkgdesc="A Virtual Globe and World Atlas that you can use to learn more about Earth"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url='https://marble.kde.org'
license="LGPL-2.1-or-later AND GPL-3.0-or-later"
depends_dev="
	gpsd-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kparts-dev
	krunner-dev
	kwallet-dev
	phonon-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtserialport-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	qt5-qtwebengine-dev
	shared-mime-info
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/education/marble.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/marble-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Requires itself to be installed

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DMOBILE=ON \
		-DBUILD_MARBLE_APPS=YES
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
33930d9b529eb11cad735636e3691733af395231cef7843738d141822eb39035a23118661b10cd576c15de6a75dce935ff5b5df0445c7c6f6283a3a859692774  marble-23.08.0.tar.xz
"
