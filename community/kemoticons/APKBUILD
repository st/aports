# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kemoticons
pkgver=5.109.0
pkgrel=0
pkgdesc="Support for emoticons and emoticons themes"
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kservice-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kemoticons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kemoticons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kemoticons.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kemoticons-kemoticontest and kemoticons-ktexttohtmlplugintest are broken
	xvfb-run ctest --test-dir build --output-on-failure -E "kemoticons-(kemoticon|ktexttohtmlplugin)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
82210b029ba1ab85bd6f356299f8c89dae3ac1f10545ee4cbeaf613f0f964c765a4e9f1b57278b9fda131941177a9d3dc9b276b411d25138c5aa7b2dd8db1d2b  kemoticons-5.109.0.tar.xz
"
