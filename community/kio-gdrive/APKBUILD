# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kio-gdrive
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> libkgapi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KIO_GDrive"
pkgdesc="KIO Slave to access Google Drive"
license="GPL-2.0-or-later"
depends="
	kaccounts-providers
	signon-plugin-oauth2
	signon-ui
	"
makedepends="
	extra-cmake-modules
	kaccounts-integration-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkgapi-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/kio-gdrive.git"
source="https://download.kde.org/stable/release-service//$pkgver/src/kio-gdrive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c5ef3a8fb78cb60ddce97fdf77326433b2e8555f2d4cd4db56dafea213b9c0050ff8ac3409807427c0c876348d36e7f4405049ead1e8a68067ef3cf8a199660b  kio-gdrive-23.08.0.tar.xz
"
