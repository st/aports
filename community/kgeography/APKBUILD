# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kgeography
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kgeography"
pkgdesc="Geography Trainer"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/education/kgeography.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgeography-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
09d8e9010fab51782c2eec6266e19c88d8e0a8745bfbfc84a39bb7c08ad0da0bef44eed832384d52d2d5a1daac616519662089af78396c2f65ea0177b282e31f  kgeography-23.08.0.tar.xz
"
