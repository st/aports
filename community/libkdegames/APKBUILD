# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkdegames
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/applications/games"
pkgdesc="Common code and data for many KDE games"
license="LGPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kbookmarks-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdnssd-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	kjobwidgets-dev
	knewstuff-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libsndfile-dev
	openal-soft-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/games/libkdegames.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdegames-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang carddecks::noarch"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

carddecks() {
	pkgdesc="Contains all carddecks for KDE cardgames"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/carddecks
}

sha512sums="
8ba0a20160bb66e44092db0be1bed81989446489a8b6b7f02773bf79cfd73f49df20a690f4cc11bf8eb20ecfc4a47a51a440b1f7843bcef5c86256ec3dbaad05  libkdegames-23.08.0.tar.xz
"
