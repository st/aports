# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kxmlgui
pkgver=5.109.0
pkgrel=0
pkgdesc="User configurable main windows"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	attica-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="
	mesa-dri-gallium
	xvfb-run
	"
_repo_url="https://invent.kde.org/frameworks/kxmlgui.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kxmlgui-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kxmlgui.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kxmlgui_unittest, ktoolbar_unittest and ktooltiphelper_unittest are broken
	LC_ALL=C xvfb-run ctest --test-dir build --output-on-failure -E '(kxmlgui|ktoolbar|ktooltiphelper)_unittest'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
60fecac53a3c0cb7228ba54ae6d40e37b5a50905cc54be70d9fde8bcf48d4aefda965393b53c1e2e31addcfb6cd3c179868e3eb2444df4a169431eb451971188  kxmlgui-5.109.0.tar.xz
"
