# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=dolphin
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/dolphin"
pkgdesc="KDE File Manager"
license="GPL-2.0-only"
depends="
	udisks2
	kio-extras
	"
depends_dev="
	baloo-dev
	baloo-widgets-dev
	kactivities-dev
	kbookmarks-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kparts-dev
	ktextwidgets-dev
	kuserfeedback-dev
	kwindowsystem-dev
	musl-fts-dev
	phonon-dev
	qt5-qtbase-dev
	solid-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	ruby-test-unit
	samurai
	"
checkdepends="
	xvfb-run
	dbus
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-zsh-completion"
_repo_url="https://invent.kde.org/system/dolphin.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-$pkgver.tar.xz
	0001-Fix-musl-build-by-using-fts-from-external-library.patch
	"

build() {
	LDFLAGS="$LDFLAGS -Wl,-z,stack-size=0x100000" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# kfileitemmodeltest, placesitemmodeltest, dolphinmainwindowtest and kitemlistcontrollertest are broken
	xvfb-run ctest --test-dir build --output-on-failure -E "(kfileitemmodel|placesitemmodel|dolphinmainwindow|kitemlistcontroller)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
6055d9f8f2148f05a0d9e68f88146e06f68ef1f2498c65a160848eb6fdc9c75490f7d00457a089bbd0b0ff592528ba24b66e35f10c1cc3d8a1b5cfebb966afba  dolphin-23.08.0.tar.xz
5b3a1095d519a2d2e875e4dc468f170fd19515963ad4946268533053785b0b3beee57ee9f8d0650de5cb5534dcc948f61327fd836908371cfccf279eea296915  0001-Fix-musl-build-by-using-fts-from-external-library.patch
"
