# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=dasel
pkgver=2.3.5
pkgrel=0
pkgdesc="Query and modify data structures using selector strings"
url="https://daseldocs.tomwright.me/"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/TomWright/dasel/archive/v$pkgver/dasel-$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X github.com/tomwright/dasel/internal.Version=$pkgver
		" ./cmd/dasel
}

check() {
	go test ./...
}

package() {
	install -Dm755 dasel -t "$pkgdir"/usr/bin/
}

sha512sums="
bec541f5898bc7b3a55c5b2c39f1e639881fe51440014d5669cd031bb98dbd8a5f384bf3a429fb771aeacde844eac3661ad690c0836b9807487501012f7129af  dasel-2.3.5.tar.gz
"
