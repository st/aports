# Maintainer: Hugo Rodrigues <hugorodrigues@hugorodrigues.xyz>
pkgname=filebrowser
pkgver=2.24.2
pkgrel=0
pkgdesc="Web File Browser"
url="https://github.com/filebrowser/filebrowser"
arch="x86_64"
license="Apache-2.0"
makedepends="go npm"
pkgusers="$pkgname"
pkggroups="$pkgname"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/filebrowser/filebrowser/archive/refs/tags/v$pkgver.tar.gz
	filebrowser.initd
	filebrowser.confd
	"

prepare() {
	default_prepare

	# Set binary version inside go
	sed -i "s/(untracked)/$pkgver/g" version/version.go
}

build() {
	cd frontend
	npm ci
	# ancient webpack
	NODE_OPTIONS="--openssl-legacy-provider" \
		npm run build

	cd "$builddir"
	go build
}

check() {
	go test ./...
}

package() {
	install -Dm755 filebrowser -t "$pkgdir"/usr/bin/
	install -Dm 644 "$srcdir"/filebrowser.confd "$pkgdir"/etc/conf.d/filebrowser
	install -Dm 755 "$srcdir"/filebrowser.initd "$pkgdir"/etc/init.d/filebrowser
	install -dDm 770 -o $pkgusers -g $pkggroups "$pkgdir"/var/lib/$pkgname
	install -dDm 770 -o $pkgusers -g $pkggroups "$pkgdir"/var/lib/$pkgname/data
}

sha512sums="
5240262f5d3c3470207f71c42c9ad3dda26dd893492184eb333f8669062fe6e7899c36cf1b499677a2d5e98b507fedb5db2f8d59c8f22471070786422406de1d  filebrowser-2.24.2.tar.gz
381cabc633f5076e46e32c5b69aca5b4c82fc227b4cbfef4d0f710db9d9a5db46dc8f29c1641f4af20fb6dc70eb33d2af63d93e194ccc2e94a6c9fae8c7f9906  filebrowser.initd
2d95fe1cef5f5a1a76448a25bed6d66d996b4a3cf6852dd7c8a63c52efc05dfff41bb7e0db20ec344c5389110852d5949131bc0a02fa3c2a48654b27a568dd7c  filebrowser.confd
"
