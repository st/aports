# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=barman
pkgver=3.7.0
pkgrel=0
pkgdesc="Backup and recovery manager for PostgreSQL"
url="http://www.pgbarman.org"
arch="noarch"
license="GPL-3.0-or-later"
depends="python3 rsync postgresql-client py3-argcomplete py3-dateutil py3-psycopg2 py3-boto3"
makedepends="py3-setuptools"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-pyc"
pkgusers="barman"
pkggroups="barman"
install="$pkgname.pre-install"
options="!check" #pytest does not start and I don't know why
checkdepends="py3-pytest-timeout py3-mock py3-pytest-runner py3-pip py3-mock"
source="$pkgname-$pkgver.tar.gz::https://github.com/EnterpriseDB/barman/releases/download/release/$pkgver/barman-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm0644 ./scripts/barman.bash_completion \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -d -o $pkgusers -g $pkggroups -m 755 "$pkgdir"/var/log/$pkgname

	cd doc
	install -Dm0644 barman.conf -t  "$pkgdir/etc"
}

sha512sums="
068b1056be2c9c9a9ccf277da25a433e2c9f71260ab68253491d7884fa5fae84b588f18f363ac07556f88304be7b2ba6b9845ed37d822b51d18e05a26712fa12  barman-3.7.0.tar.gz
"
