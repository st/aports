# Contributor: Orion <systmkor@gmail.com>
# Maintainer: Orion <systmkor@gmail.com>
pkgname=py3-scrapy
pkgver=2.10.0
pkgrel=0
pkgdesc="Fast high-level scraping and web crawling framework"
url="https://scrapy.org/"
arch="noarch"
license="BSD-3-Clause"
depends="
	py3-cryptography
	py3-cssselect
	py3-itemadapter
	py3-itemloaders
	py3-lxml
	py3-openssl
	py3-parsel
	py3-protego
	py3-pydispatcher
	py3-queuelib
	py3-service_identity
	py3-tldextract
	py3-twisted
	py3-w3lib
	py3-zope-interface
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-pyftpdlib
	py3-pytest
	py3-sybil
	py3-testfixtures
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/S/Scrapy/Scrapy-$pkgver.tar.gz"
builddir="$srcdir"/Scrapy-$pkgver
options="!check" # take forever

# secfixes:
#   2.6.1-r0:
#     - CVE-2022-0577
#     - CVE-2022-21716

replaces="py-scrapy" # Backwards compatibility
provides="py-scrapy=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest tests
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
59338b4f00285c07f7bddfb0c2c257a8ee86cb557edf643f388581b90c90051072df03996f94822e2f8cc51ade312cedef8885776a4313ee20d23ee16d879eaa  Scrapy-2.10.0.tar.gz
"
