# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Fraser Waters <frassle@gmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi
pkgver=3.79.0
pkgrel=0
pkgdesc="Infrastructure as Code SDK"
url="https://pulumi.com/"
# Tests show that pulumi's plugin system doesn't work on any other platforms
arch="x86_64 aarch64"
license="Apache-2.0"
makedepends="go"
checkdepends="tzdata"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-language-go:_go
	$pkgname-language-nodejs:_nodejs
	$pkgname-language-python:_python
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi/archive/v$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="-X github.com/pulumi/pulumi/sdk/v${pkgver%%.*}/go/common/version.Version=v$pkgver"
	mkdir -p "$builddir"/bin

	cd "$builddir"/pkg
	go build -v \
		-o "$builddir"/bin/pulumi \
		-ldflags "-X github.com/pulumi/pulumi/pkg/v${pkgver%%.*}/version.Version=v$pkgver" \
		./cmd/pulumi

	cd "$builddir"/sdk
	go build -v \
		-o "$builddir"/bin/pulumi-language-python \
		-ldflags "$_goldflags" \
		./python/cmd/pulumi-language-python

	cd "$builddir"/sdk/nodejs/cmd/pulumi-language-nodejs
	go build -v \
		-o "$builddir"/bin/pulumi-language-nodejs \
		-ldflags "$_goldflags"

	cd "$builddir"/sdk/go
	go generate ./pulumi/...
	cd pulumi-language-go
	go build -v \
		-o "$builddir"/bin/pulumi-language-go \
		-ldflags "$_goldflags"

	cd "$builddir"
	for shell in bash fish zsh; do
		./bin/pulumi gen-completion $shell > pulumi.$shell
	done
}

check() {
	cd "$builddir"/pkg

	# codegen tests require runtime dependencies (python, yarn, dotnet-runtime).
	# integration tests require node, and hardcode pulumi/ as the root path to search for sdk/go.mod file.
	# httpstate tests fail with microseconds difference in time.
	# TestPluginMapper_MappedNamesDifferFromPulumiName fails occationally (seems like the AWS provider plugin isn't cached as intended sometimes)
	# shellcheck disable=2046
	go test \
		-skip TestPluginMapper_MappedNamesDifferFromPulumiName \
		$(go list ./... | grep -v \
		-e 'github.com/pulumi/pulumi/pkg/v3/backend/httpstate' \
		-e 'github.com/pulumi/pulumi/pkg/v3/codegen/dotnet' \
		-e 'github.com/pulumi/pulumi/pkg/v3/codegen/go' \
		-e 'github.com/pulumi/pulumi/pkg/v3/codegen/python' \
		-e 'github.com/pulumi/pulumi/pkg/v3/codegen/nodejs' \
		-e 'github.com/pulumi/pulumi/pkg/v3/testing/integration' \
		)
}

package() {
	install -Dm755 bin/pulumi -t "$pkgdir"/usr/bin/

	install -Dm644 pulumi.bash \
		"$pkgdir"/usr/share/bash-completion/completions/pulumi
	install -Dm644 pulumi.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/pulumi.fish
	install -Dm644 pulumi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_pulumi
}

_go() {
	pkgdesc="$pkgdesc (Go language provider)"
	depends="$pkgname=$pkgver-r$pkgrel"

	install -Dm755 "$builddir"/bin/pulumi-language-go -t "$subpkgdir"/usr/bin/
}

_nodejs() {
	pkgdesc="$pkgdesc (NodeJS language provider)"
	depends="$pkgname=$pkgver-r$pkgrel"

	install -Dm755 "$builddir"/bin/pulumi-language-nodejs -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/nodejs/dist/pulumi-analyzer-policy \
		-t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/nodejs/dist/pulumi-resource-pulumi-nodejs \
		-t "$subpkgdir"/usr/bin/
}

_python() {
	pkgdesc="$pkgdesc (Python language provider)"
	depends="$pkgname=$pkgver-r$pkgrel python3"

	install -Dm755 "$builddir"/bin/pulumi-language-python -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/cmd/pulumi-language-python-exec -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/dist/pulumi-analyzer-policy-python \
		-t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/dist/pulumi-resource-pulumi-python \
		-t "$subpkgdir"/usr/bin/
}

sha512sums="
1c2a5dc40e276e27af166faaf5bb091ba4a6ee8285ea3008ed00ae980e8ee8571caff533964f677dcb6551ba3a2e77cf9eb4ea35c417c070431536b470858e38  pulumi-3.79.0.tar.gz
"
