# Contributor: Maxim Karasev <mxkrsv@disroot.org>
# Maintainer: Maxim Karasev <mxkrsv@disroot.org>
pkgname=sqlmap
pkgver=1.7.8
pkgrel=0
pkgdesc="Automatic SQL injection and database takeover tool"
url="https://sqlmap.org/"
arch="noarch"
license="GPL-2.0-or-later"
depends="python3 py3-six py3-beautifulsoup4 py3-keepalive py3-termcolor
	py3-colorama py3-bottle py3-chardet py3-magic py3-pydes"
makedepends="py3-setuptools"
subpackages="$pkgname-pyc"
source="https://pypi.python.org/packages/source/s/sqlmap/sqlmap-$pkgver.tar.gz
	reduce-amount-of-third-party-dependencies.patch"
options="!check" # fail with py311 for some reason

prepare() {
	# remove unneeded thirdparty moduels (keep patch small)
	cd sqlmap/thirdparty
	rm -rf chardet bottle beautifulsoup colorama keepalive magic odict \
		pydes six termcolor

	# pip sources hierarchy is not exactly the same as git
	local builddir="$builddir"/sqlmap
	default_prepare
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install \
		--prefix=/usr \
		--root="$pkgdir" \
		--single-version-externally-managed
}

sha512sums="
8440f135176e3b87a86a3acff12a94ed7fe545789072f4d7b7a52ab007756e097b2478d7d700f14b60d9dd884c41c52cd9d77fdb812d6d1d31391c5cf0aad530  sqlmap-1.7.8.tar.gz
95e72a54f98b0906d95c9f4d9022782bf32c04ed099afc4fd449ba7d1b717954c658511883e5d8b62b38498ca9b5a81927758e8d21a81aac82619ef4f62ee053  reduce-amount-of-third-party-dependencies.patch
"
