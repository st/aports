# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=raspberrypi-bootloader
# To match Alpine kernel schedule, use master branch commit id rather than older stable tagged releases
# Keep by-the-date release numbering for consistency
_commit=a88533edd6d3c0ea93ddd5bb5c89f47ec8fb6e98
pkgver=1.20230811
pkgrel=0
pkgdesc="Bootloader files for the Raspberry Pi"
url="https://github.com/raspberrypi/rpi-firmware"
arch="armhf armv7 aarch64"
license="custom"
options="!check !strip !tracedeps !spdx"
source="$pkgname-$pkgver.tar.gz::https://github.com/raspberrypi/rpi-firmware/archive/$_commit.tar.gz"
subpackages="$pkgname-common $pkgname-experimental $pkgname-debug $pkgname-cutdown $pkgname-doc"
depends="$pkgname-common=$pkgver-r$pkgrel"

builddir="$srcdir/rpi-firmware-$_commit"

package() {
	local fw; for fw in bootcode.bin fixup.dat fixup4.dat start.elf start4.elf; do
		install -D "$builddir"/$fw \
			"$pkgdir"/boot/$fw
	done
	install -Dm 644 "$builddir"/LICENCE.broadcom \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

common() {
	pkgdesc="Common files used by Raspberry Pi bootloaders"
	depends=
	amove boot/bootcode.bin
}

experimental() {
	pkgdesc="Experimental firmware with additional codecs"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_x.elf start4x.elf fixup_x.dat fixup4x.dat; do
		install -D "$builddir"/$fw \
				"$subpkgdir"/boot/$fw
	done
}

debug() {
	pkgdesc="Debug firmware"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_db.elf start4db.elf fixup_db.dat fixup4db.dat; do
		install -D "$builddir"/$fw \
			"$subpkgdir"/boot/$fw
	done
}

cutdown() {
	pkgdesc="Cut-down firmware for lower memory settings"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_cd.elf start4cd.elf fixup_cd.dat fixup4cd.dat; do
		install -D "$builddir"/$fw \
			"$subpkgdir"/boot/$fw
	done
}

sha512sums="
3330dca6fc30262d307fb1ec96755e61036c715608fe3816f017c2a237df539620c1f2ea7ab570f5b64450e418a3cdf5fdaf104d8a2685d23d5e3b7b19307a8d  raspberrypi-bootloader-1.20230811.tar.gz
"
